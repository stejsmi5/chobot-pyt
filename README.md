## Systém pro správu neuronových sítí

Tento systém umožňuje jednoduchým způsobem vytváření a správu neuoronových sítí běžících v Kubernetes clusteru.
Uživatel definuje ve webovém rozhraní aplikace, jaký problém potřebujě řešit (klasifikace obrázů, logů, či jednoduchý chatbot), pokud je potřeba, dodá trénovací data a následně je mu vytvořen nový kontejner s naučenou neuronovou sítí, na kterou lze odesílat dotazy a která vrátí odpověď v podobě třídy klasifikace.

Pro každou třídu klasifikace každé neuronové sítě půjde dále vytvořit modul, na který bude odeslán dotaz přímo ze sítě, dojde li ke splnění definovaných podmínek. 

![diagram1](./docs/diagram1.png)

Uživatel pomocí frontend aplikace (není součástí sp) vybere jaký problém chce řešit. Po odeslání dat na **container api** je pomocí kubernetes api vytvořen nový deployment a service, které jsou pro jednoduché routování doplněni amabasador anotace.
Dále uživatel vybere pro jakou třídu klasifikace chce vytvořit modul, a předá zdrojový kód. Tento zdrojový kód je předán nejdříve na **container api**, které jej zvaliduje a následně pošle požadavek na **image builder** který tento kód spojí společně s připravenou šablonou a následně z něj vytvoří docker image. Tento image je následně nahrán do **docer registru** Poté je opět vytvořen nový deploiment s modulem, který běží pod ambasadorem. 

Uživateli je sdělena uri na kterou může odesílat požadavky a síť jednotlivé požadavky ohodnotí a pokud je pro danou třídu klasifikace vytvořen modul, odešle po klasifikaci síť dotaz na daný modul a společně s jeho výsledkem předá data zpět uživateli.

### Chobot container api (součást sp)
Api přijímá od uživatele 2 druhy požadavků. Vytvoření sítě a vytvoření modulu. Pokud se jedná o vytvoření sítě, api převede požadavek uživatele na kubernetes deployment api object, který následně zašle na kubernetes api. 

Pokud se jedná o požadavek na vytvoření nového modulu. Je zdorjový kód modulu předán **image builderu** a po vytvoření nového docker image je tento image deployván stejně jako neuronová síť.

Api dále slouží k získávání dat o sítích či modulech, uživatel je schopen pomocí api získat aktuální status modulu nebo například vypsat poslední logy. 


### Chobot image builder (součást sp)
Api přijme data od **container api** a podle parametrů rozhodne, zdali se jedná o docker image, git repo, nebo uživatelský kód. Poku se jedná o zdrojový kód, stáhne image builder připravenou šablonu obsahující api s endpointy pro modul a spojí je společně se zdrojovým kódem. Následně takto spojený kód zbuildí do docker image, který je nahrán do privátního docker registru.  

### Frontend (není součást sp)
Frontend aplikace slouží k základní obsluze **container api** kde uživatel může vytvářet nové sítě a moduly. Psát zdrojový kód modulů, nebo získavat informace o sítích a modulech (status, logy..)

